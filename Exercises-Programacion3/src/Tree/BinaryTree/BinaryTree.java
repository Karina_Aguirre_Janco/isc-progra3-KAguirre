package Tree.BinaryTree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree<T extends Comparable<T>> implements IBinaryTree<T> {
    private TreeNode<T> rootNode;
    private int height;
    private int weight;
    private int numberOfNodes;

    public BinaryTree() { this.rootNode = null; }

    public BinaryTree(TreeNode<T> rootNode) {
        this.rootNode = rootNode;
        this.height = 0;
        this.weight = 0;
        this.numberOfNodes = 0;
    }

    public TreeNode<T> getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode<T> rootNode) {
        this.rootNode = rootNode;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public void insertNode(T data) {
        TreeNode<T> newNode = new TreeNode<>(data);

        if (rootNode == null) {
            rootNode = newNode;
            return;
        }

        Queue<TreeNode<T>> queue = new LinkedList<>();
        queue.offer(rootNode);
        while (!queue.isEmpty()) {
            TreeNode<T> currentNode = queue.poll();
            if (currentNode.getLeftNode() == null) {
                currentNode.setLeftNode(newNode);
                return;
            } else if (currentNode.getRightNode() == null) {
                currentNode.setRightNode(newNode);
                return;
            } else {
                queue.offer(currentNode.getLeftNode());
                queue.offer(currentNode.getRightNode());
            }
        }
    }

    @Override
    public void traverseIterativeBFS() {
        if (rootNode == null) {
            return;
        }

        Queue<TreeNode<T>> queue = new LinkedList<>();
        queue.add(rootNode);

        while (!queue.isEmpty()) {
            int levelSize = queue.size();

            for (int i = 0; i < levelSize; i++) {
                TreeNode<T> node = queue.poll();
                System.out.print(node.getData() + " - ");

                if (node.getLeftNode() != null) {
                    queue.add(node.getLeftNode());
                }
                if (node.getRightNode() != null) {
                    queue.add(node.getRightNode());
                }
            }
        }
    }

    @Override
    public boolean isBinarySearchTree() {
        return isBST(rootNode, null, null);
    }

    private boolean isBST(TreeNode<T> node, T min, T max) {
        if (node == null) {
            return true;
        }
        if ((min != null && node.getData().compareTo(min) <= 0) ||
                (max != null && node.getData().compareTo(max) >= 0)) {
            return false;
        }
        return isBST(node.getLeftNode(), min, node.getData()) && isBST(node.getRightNode(), node.getData(), max);
    }


    public void inorder() { inorderRec(this.rootNode); }
    // A utility function to
    // do inorder traversal of BST
    private void inorderRec(TreeNode<T> root) {
        if (root != null) {
            inorderRec(root.getLeftNode());
            //System.out.println(root.getData());
            inorderRec(root.getRightNode());
        }
    }

    @Override
    public void deleteNode(TreeNode node) {}

    @Override
    public boolean findNode(TreeNode node) {
        return false;
    }

    @Override
    public void print() {}

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public void printBFS() {}
    @Override
    public void printDFS() {}
    @Override
    public void printPreOrderIterativeDFS() {
        Stack<TreeNode> stack = new Stack<>();
        stack.push(this.rootNode);

        while (!stack.isEmpty()) {
            TreeNode<T> currentNode = stack.pop();
            System.out.print("Node: " + currentNode.getData() + " - ");

            if (currentNode.getRightNode() != null) {
                System.out.print("Right child: " + currentNode.getRightNode().getData() + " - ");
                stack.push(currentNode.getRightNode());
            }

            if (currentNode.getLeftNode() != null) {
                System.out.print("Left child: " + currentNode.getLeftNode().getData() + " - ");
                stack.push(currentNode.getLeftNode());
            }
            System.out.println();
        }
    }
    @Override
    public Iterator<ITreeNode<T>> iterator() {
        return null;
    }
    @Override
    public int getNumberOfNodes() {
        return 0;
    }
    @Override
    public int height() {
        return 0;
    }
}