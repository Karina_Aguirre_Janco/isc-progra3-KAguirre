package Tree.BinaryTree;

public class TreeNode<T> implements ITreeNode<T> {
    private TreeNode leftNode;
    private TreeNode rightNode;
    private TreeNode parentNode;
    private int level;
    private T data;

    public TreeNode(TreeNode leftChild, TreeNode rightChild, T data) {
        this.leftNode = leftChild;
        this.rightNode = rightChild;
        this.data = data;
    }

    public TreeNode(T data) {
        this.leftNode = this.rightNode = null;
        this.data = data;
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public void setData(T d) {
        data = d;
    }

    public void setLeftNode(TreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public void setRightNode(TreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public void setParentNode(TreeNode parentNode) {
        this.parentNode = parentNode;
    }

    @Override
    public TreeNode<T> getLeftNode() {
        return leftNode;
    }

    @Override
    public void setLeft(TreeNode<T> leftNode) {
        leftNode = leftNode;
    }

    @Override
    public TreeNode<T> getRightNode() {
        return rightNode;
    }

    @Override
    public void setRight(TreeNode<T> rightNode) {
        rightNode = rightNode;
    }

    @Override
    public int getLevel() {
        return level;
    }

    public TreeNode getParentNode() {
        return parentNode;
    }
}
