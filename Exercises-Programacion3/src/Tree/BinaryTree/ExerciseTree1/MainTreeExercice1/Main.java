package Tree.BinaryTree.ExerciseTree1.MainTreeExercice1;

import Tree.BinaryTree.BinaryTree;

public class Main {
    public static void main(String[] args) {
        // Exercise 1 method iterative
        BinaryTree<String> binaryTree = new BinaryTree<>();
        binaryTree.insertNode("E");
        binaryTree.insertNode("C");
        binaryTree.insertNode("D");
        binaryTree.insertNode("A");
        binaryTree.insertNode("B");
        binaryTree.insertNode("F");
        binaryTree.insertNode("G");
        System.out.println("---------------Exercise 1 | a------------");
        binaryTree.traverseIterativeBFS();
        System.out.println("\n---------------Exercise 1 | b------------");
        if (binaryTree.isBinarySearchTree()){
            System.out.println("It is a binary tree search");
        } else
            System.out.println("It isn't a binary tree search");
    }
}
