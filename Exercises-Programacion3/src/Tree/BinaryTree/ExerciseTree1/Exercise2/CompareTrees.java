package Tree.BinaryTree.ExerciseTree1.Exercise2;

import Tree.BinaryTree.BinaryTree;
import Tree.BinaryTree.TreeNode;
import Tree.GeneralTree.GeneralTree;
import Tree.GeneralTree.Node;

import java.util.List;

public class CompareTrees {
    public boolean compareTo(GeneralTree tree_one, GeneralTree tree_two, StringBuilder diff){
        if (tree_one == null && tree_two == null) {
            return true;
        }
        if (tree_one == null || tree_two == null) {
            diff.append("Null trees");
            return false;
        }

        // Comparing the root value of the trees
        if (!tree_one.getRoot().getData().equals(tree_two.getRoot().getData())) {
            diff.append("Values are different: ")
                    .append(tree_one.getRoot().getData())
                    .append(" != ")
                    .append(tree_two.getRoot().getData());
            return false;
        }

        // Comparing the children of the trees
        List<Node> children_one = tree_one.getRoot().getChildren();
        List<Node> children_two = tree_two.getRoot().getChildren();
        if (children_one.size() != children_two.size()) {
            diff.append("Values are different: ")
                    .append(children_one.size())
                    .append(" != ")
                    .append(children_two.size());
            System.out.println("The structure are different");
            return false;
        }
        for (int i = 0; i < children_one.size(); i++) {
            if (!compareTo(new GeneralTree<>(children_one.get(i)), new GeneralTree<>(children_two.get(i)), diff)) {
                System.out.println(diff);
                return false;
            }
        }
        return true;
    }

    public boolean compareTo(BinaryTree tree_one, BinaryTree tree_two){
        return compareNodes(tree_one.getRootNode(), tree_two.getRootNode());
    }

    private boolean compareNodes(TreeNode node_one, TreeNode node_two) {
        if (node_one == null && node_two == null) {
            return true;
        } else if (node_one == null || node_two == null) {
            System.out.println("Trees are different: one node is null");
            return false;
        } else if (!node_one.getData().equals(node_two.getData())) {
            System.out.println("Trees are different: " + node_one.getData() +" != "+node_two.getData());
            return false;
        } else {
            boolean left = compareNodes(node_one.getLeftNode(), node_two.getLeftNode());
            boolean right = compareNodes(node_one.getRightNode(), node_two.getRightNode());
            return left && right;
        }
    }
}
