package Tree.BinaryTree.ExerciseTree1.Exercise2;

import Tree.BinaryTree.BinaryTree;
import Tree.GeneralTree.GeneralTree;
import Tree.GeneralTree.Node;

public class Main {
    public static void main(String[] args) {
        CompareTrees compareTrees = new CompareTrees();
        Node<Integer> root = new Node<>(1, null);
        Node<Integer> child1 = new Node<>(2, root);
        Node<Integer> child2 = new Node<>(3, root);

        root.addChild(child1);
        root.addChild(child2);
        GeneralTree tree = new GeneralTree<>(root);

        Node<Integer> root2 = new Node<>(1, null);
        Node<Integer> child1_2 = new Node<>(2, root2);
        Node<Integer> child2_2 = new Node<>(3, root2);

        root2.addChild(child1_2);
        root2.addChild(child2_2);
        GeneralTree tree2 = new GeneralTree<>(root2);

        System.out.println("Example_1");
        System.out.println(compareTrees.compareTo(tree,tree2, new StringBuilder()));

        Node<Integer> root3 = new Node<>(1, null);
        Node<Integer> child1_3 = new Node<>(3, root3);
        Node<Integer> child2_3 = new Node<>(3, root3);

        root3.addChild(child1_3);
        root3.addChild(child2_3);
        GeneralTree tree3 = new GeneralTree<>(root3);

        System.out.println("Example_2");
        System.out.println(compareTrees.compareTo(tree3,tree,new StringBuilder()));

        Node<Integer> root4 = new Node<>(1, null);
        Node<Integer> child1_4 = new Node<>(3, root4);
        Node<Integer> child2_4 = new Node<>(3, root4);
        child1_4.addChild(child2_4);

        root4.addChild(child1_4);
        GeneralTree tree4 = new GeneralTree<>(root4);

        System.out.println("Example_3");
        System.out.println(compareTrees.compareTo(tree,tree4,new StringBuilder()));
        System.out.println("----------Compare to with Binary tree---------------");
        BinaryTree<Integer> binaryTree = new BinaryTree<>();
        binaryTree.insertNode(1);
        binaryTree.insertNode(2);
        binaryTree.insertNode(3);

        BinaryTree<Integer> binaryTree2 = new BinaryTree<>();
        binaryTree2.insertNode(1);
        binaryTree2.insertNode(2);
        binaryTree2.insertNode(3);

        System.out.println("Example 1");
        System.out.println(compareTrees.compareTo(binaryTree,binaryTree2));

        System.out.println("Example 2");
        BinaryTree<Integer> binaryTree3 = new BinaryTree<>();
        binaryTree3.insertNode(1);
        binaryTree3.insertNode(3);
        binaryTree3.insertNode(3);
        System.out.println(compareTrees.compareTo(binaryTree3,binaryTree));
    }
}
