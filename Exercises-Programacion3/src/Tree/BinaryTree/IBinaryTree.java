package Tree.BinaryTree;

import java.util.Iterator;

public interface IBinaryTree<T> {
    void insertNode(T node);
    void traverseIterativeBFS();
    boolean isBinarySearchTree();
    void deleteNode(TreeNode<T> node);
    boolean findNode(TreeNode<T> node);
    void print();
    int getWeight();
    int getHeight();
    void printBFS(); // preorder o postorder
    void printDFS(); // preorder o postorder
    void printPreOrderIterativeDFS();
    Iterator<ITreeNode<T>> iterator();
    int getNumberOfNodes();
    int height();
}