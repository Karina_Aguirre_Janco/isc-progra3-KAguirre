package Tree.BinaryTree;

public class Main {
    public static void main(String args[]) {
        TreeNode<Integer> rootNode = new TreeNode<>(40);

        BinaryTree<Integer> binaryTree = new BinaryTree<>();
        //       40
        //    8      6
        // 13  12  5  18

        // create nodes of the tree
        TreeNode<Integer> node1 = new TreeNode<>(8);
        TreeNode<Integer> node2 = new TreeNode<>(6);
        TreeNode<Integer> node3 = new TreeNode<>(13);
        TreeNode<Integer> node4 = new TreeNode<>(12);
        TreeNode<Integer> node5 = new TreeNode<>(5);
        TreeNode<Integer> node6 = new TreeNode<>(18);

        binaryTree.insertNode(40);
        binaryTree.insertNode(8);
        binaryTree.insertNode(6);
        binaryTree.insertNode(13);
        binaryTree.insertNode(12);
        binaryTree.insertNode(5);
        binaryTree.insertNode(18);

        binaryTree.printPreOrderIterativeDFS();//40 - 8 - 13 - 12 - 6 - 5 - 18
        binaryTree.inorder();
    }
}
