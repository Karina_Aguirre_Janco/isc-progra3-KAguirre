package Tree.BinaryTree.binarySearchTree;

public class Main {
    public static void main(String[] args) {

        BinarySearchTree binarySearchTree = new BinarySearchTree();

        binarySearchTree.insert(30);
        binarySearchTree.insert(9);
        binarySearchTree.insert(40);
        binarySearchTree.insert(20);
        binarySearchTree.insert(7);
        binarySearchTree.insert(38);
        binarySearchTree.insert(42);

        //       30
        //   9       40
        // 7  20   38  42

        binarySearchTree.printInOrderIterativeDFS(); // 7 - 9 - 20 - 30 - 38 - 40 - 42
        System.out.println();
        binarySearchTree.printInOrderRecursiveDFS();
        System.out.println();
        System.out.println(binarySearchTree.search(31));
    }
}
