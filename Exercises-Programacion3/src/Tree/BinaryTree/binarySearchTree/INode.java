package Tree.BinaryTree.binarySearchTree;

public interface INode <T> {
    T getData();
    void setData(T d);

    INode<T> getLeftNode();
    void setLeftNode(INode<T> leftNode);

    INode<T> getRightNode();
    void setRightNode(INode<T> rightNode);

    T getLevel();
}
