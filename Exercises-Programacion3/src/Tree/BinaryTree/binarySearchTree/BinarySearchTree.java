package Tree.BinaryTree.binarySearchTree;

import Tree.BinaryTree.ITreeNode;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinarySearchTree<T extends Comparable<T>> implements IBinarySearchTree<T> {
    private INode<T> root;
    public BinarySearchTree() {
        this.root = null;
    }

    public void insert(T newData) {
        this.root = insertRecursive(root, newData);
    }
    private INode<T> insertRecursive(INode<T> root, T newData) {
        INode<T> newNode = new Node<>(newData);

        if (root == null) {
            root = newNode;
            return root;
        }

        INode<T> currentNode = root;
        while (true) {
            if (newData.compareTo(currentNode.getData()) <= 0) {
                if (currentNode.getLeftNode() == null) {
                    currentNode.setLeftNode(newNode);
                    break;
                } else {
                    currentNode = currentNode.getLeftNode();
                }
            } else {
                if (currentNode.getRightNode() == null) {
                    currentNode.setRightNode(newNode);
                    break;
                } else {
                    currentNode = currentNode.getRightNode();
                }
            }
        }
        return root;
    }

    @Override
    public void delete(Node<T> node) {
        this.root = deleteRecursive(root, node.getData());
    }

    private INode<T> deleteRecursive(INode<T> current, T data) {
        if (current == null) {
            return null;
        }

        if (data.compareTo(current.getData()) == 0) {
            if (current.getLeftNode() == null && current.getRightNode() == null) {
                return null;
            }
            if (current.getLeftNode() == null) {
                return current.getRightNode();
            }
            if (current.getRightNode() == null) {
                return current.getLeftNode();
            }
            INode<T> inOrderPredecessor = findInOrderPredecessor(current.getLeftNode());
            current.setData(inOrderPredecessor.getData());
            current.setLeftNode(deleteRecursive(current.getLeftNode(), inOrderPredecessor.getData()));
        } else if (data.compareTo(current.getData()) < 0) {
            current.setLeftNode(deleteRecursive(current.getLeftNode(), data));
        } else {
            current.setRightNode(deleteRecursive(current.getRightNode(), data));
        }
        return current;
    }

    private INode<T> findInOrderPredecessor(INode<T> node) {
        while (node.getRightNode() != null) {
            node = node.getRightNode();
        }
        return node;
    }

    @Override
    public boolean search(T node) {
        return search(root, new Node<>(node));
    }

    private boolean search(INode<T> current, INode<T> node){
        if (current == null){
            return false;
        }
        else {
            if (current.getData().compareTo(node.getData()) == 0) return true;

            else if (node.getData().compareTo(current.getData()) < 0) {
                return search(current.getLeftNode(), node);
            }
            else if (node.getData().compareTo(current.getData()) > 0){
                return search(current.getRightNode(), node);
            }

            return false;
        }
    }

    @Override
    public void print() {

    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public int getHeight() {
        return getHeightRecursive(root);
    }

    private int getHeightRecursive(INode<T> node) {
        if (node == null) {
            return 0;
        } else {
            int leftHeight = getHeightRecursive(node.getLeftNode());
            int rightHeight = getHeightRecursive(node.getRightNode());
            return Math.max(leftHeight, rightHeight) + 1;
        }
    }

    @Override
    public void printBFS() {

    }

    @Override
    public void printDFS() {

    }

    @Override
    public void printPreOrderIterativeDFS() {

    }
    public void printPreOrderRecursiveDFS() {
        printPreOrderRecursiveDFS(root);
    }

    @Override
    public void printInOrderRecursiveDFS() {
        printInOrderRecursiveDFS(root);
    }

    public void printInOrderRecursiveDFS(INode<T> node) {
        if (node != null) {
            printInOrderRecursiveDFS(node.getLeftNode());
            System.out.print(node.getData() + " - ");
            printInOrderRecursiveDFS(node.getRightNode());
        }
    }

    @Override
    public void printInOrderIterativeDFS() {
        Stack<INode<T>> stack =  new Stack<>();
        INode<T> currentNode = this.root;

        while (currentNode != null || !stack.isEmpty()) {
            while (currentNode != null) {
                stack.push(currentNode);
                currentNode = currentNode.getLeftNode();
            }

            INode<T> n = stack.pop();
            System.out.print(n.getData() + " - ");
            currentNode = n.getRightNode();
        }
    }

    @Override
    public void traverseIterativeBFS() {
        if (root == null) {
            return;
        }

        Queue<INode<T>> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            int levelSize = queue.size();

            for (int i = 0; i < levelSize; i++) {
                INode<T> node = queue.poll();
                System.out.print(node.getData() + " ");

                if (node.getLeftNode() != null) {
                    queue.add(node.getLeftNode());
                }
                if (node.getRightNode() != null) {
                    queue.add(node.getRightNode());
                }
            }
        }
    }

    @Override
    public Iterator<ITreeNode> iterator() {
        return null;
    }

    @Override
    public int getNumberOfNodes() {
        return 0;
    }

    @Override
    public int height() {
        return 0;
    }

    private void printPreOrderRecursiveDFS(INode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getData() + " ");
        printPreOrderRecursiveDFS(root.getLeftNode());
        printPreOrderRecursiveDFS(root.getRightNode());
    }
}