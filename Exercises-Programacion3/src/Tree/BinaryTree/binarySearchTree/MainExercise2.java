package Tree.BinaryTree.binarySearchTree;

public class MainExercise2 {
    public static void main(String[] args) {
        BinarySearchTree<String> binarySearchTree = new BinarySearchTree<>();
        binarySearchTree.insert("Tres");
        binarySearchTree.insert("tristes");
        binarySearchTree.insert("tigres");
        binarySearchTree.insert("comian");
        binarySearchTree.insert("trigo");

        binarySearchTree.traverseIterativeBFS();
    }
}
