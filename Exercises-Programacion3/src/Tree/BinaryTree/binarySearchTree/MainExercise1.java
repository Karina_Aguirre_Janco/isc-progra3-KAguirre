package Tree.BinaryTree.binarySearchTree;

public class MainExercise1 {
    public static void main(String[] args) {
        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>();
        System.out.println("Example 1");
        binarySearchTree.insert(45);
        binarySearchTree.insert(10);
        binarySearchTree.insert(90);
        binarySearchTree.insert(7);
        binarySearchTree.insert(12);
        binarySearchTree.insert(50);

        Node<Integer> nodeDelete = new Node<>(12);
        binarySearchTree.delete(nodeDelete);
        binarySearchTree.traverseIterativeBFS();

        System.out.println("\nThe height is: "+binarySearchTree.getHeight());

        System.out.println("Example 2");
        BinarySearchTree<Integer> binarySearchTree2 = new BinarySearchTree<>();
        binarySearchTree2.insert(15);
        binarySearchTree2.insert(10);
        binarySearchTree2.insert(20);
        binarySearchTree2.insert(8);
        binarySearchTree2.insert(12);
        binarySearchTree2.insert(18);
        binarySearchTree2.insert(30);
        binarySearchTree2.insert(16);
        binarySearchTree2.insert(19);

        Node<Integer> nodeDelete2 = new Node<>(20);
        binarySearchTree2.delete(nodeDelete2);
        binarySearchTree2.traverseIterativeBFS();
        System.out.println("\nThe height is: "+binarySearchTree2.getHeight());
    }
}
