package Tree.BinaryTree.binarySearchTree;

import Tree.BinaryTree.ITreeNode;
import java.util.Iterator;

public interface IBinarySearchTree<T> {
    void insert(T node);
    void delete(Node<T> node);

    //    TreeNode<T> findNode(TreeNode<T> node);
    boolean search(T node);

    void print();

    int getWeight();
    int getHeight();

    void printBFS(); // preorder o postorder
    void printDFS(); // preorder o postorder

    void printPreOrderIterativeDFS();
    void printPreOrderRecursiveDFS();

    void printInOrderRecursiveDFS();
    void printInOrderIterativeDFS();
    void traverseIterativeBFS();

    Iterator<ITreeNode> iterator();

    int getNumberOfNodes();
    int height();
}
