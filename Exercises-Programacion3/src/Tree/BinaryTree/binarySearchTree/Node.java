package Tree.BinaryTree.binarySearchTree;

public class Node<T> implements INode<T> {
    //instance variable of Node class
    private T data;
    private INode<T> left;
    private INode<T> right;

    //constructor
    public Node(T data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public T getData() {
        return data;
    }

    @Override
    public void setData(T d) {
        this.data = d;
    }

    @Override
    public INode getLeftNode() {
        return this.left;
    }

    @Override
    public INode getRightNode() {
        return this.right;
    }

    @Override
    public T getLevel() {
        return null;
    }

    public void setLeftNode(INode left) {
        this.left = left;
    }

    public void setRightNode(INode right) {
        this.right = right;
    }
}