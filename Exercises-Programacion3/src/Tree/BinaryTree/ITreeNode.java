package Tree.BinaryTree;

public interface ITreeNode<T> {
    T getData();
    void setData(T d);

    TreeNode<T> getLeftNode();
    void setLeft(TreeNode<T> leftNode);

    TreeNode<T> getRightNode();
    void setRight(TreeNode<T> rightNode);

    int getLevel();
}
