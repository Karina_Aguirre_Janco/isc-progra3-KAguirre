package Tree.GeneralTree;

public class Main {
    public static void main(String[] args) {
        GeneralTree tree;

        Node<String> root = new Node<>("CEO", null);

        Node<String> managerNode1 = new Node<>("Manager-Pedro", root);
        Node<String> managerNode2 = new Node<>("Manager-Maria", root);

        Node<String> employeeNode1 = new Node<>("Engineer-Eduard",managerNode2);
        managerNode2.addChild(employeeNode1);

        Node<String> employeeNode2 = new Node<>("Engineer-Gaby",managerNode2);
        managerNode2.addChild(employeeNode2);

        Node<String> employeeNode3 = new Node<>("Engineer-Salet",managerNode2);
        managerNode2.addChild(employeeNode3);

        Node<String> employeeNode4 = new Node<>("Engineer-Gabriel",managerNode1);
        managerNode1.addChild(employeeNode4);

        root.addChild(managerNode1);
        root.addChild(managerNode2);

        tree = new GeneralTree<>(root);
        System.out.println(tree);

        System.out.println(employeeNode4.isLeaf());//true
        System.out.println(managerNode1.isLeaf());//false
    }

}