package Tree.GeneralTree;

public class GeneralTree<T> {
    private Node<T> root;
    private int height;
    private int levels;

    public GeneralTree(Node<T> root) {
        this.root = root;
    }

    public String toString() {
        return root.toString();
    }

    public Node<T> getRoot() {
        return root;
    }
}
