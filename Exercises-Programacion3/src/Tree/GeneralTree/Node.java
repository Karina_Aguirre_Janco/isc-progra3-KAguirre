package Tree.GeneralTree;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
    private T data;
    private List<Node<T>> children;
    private Node<T> father;

    public Node(T data, Node<T> father) {
        this.data = data;
        this.father = father;
        this.children = new ArrayList<>();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setFather(Node<T> father) {
        this.father = father;
    }

    public Node<T> getFather() {
        return father;
    }

    public void addChild(Node<T> child) {
        children.add(child);
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public boolean isFather() {
        return !children.isEmpty();
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getData());

        if(this.isFather()) {
            sb.append(" ( ");
            for (Node<T> c: this.getChildren()) {
                sb.append(c.toString());
                sb.append(" , ");
            }
            sb.append(" ) ");
        }

        return sb.toString();
    }
}
