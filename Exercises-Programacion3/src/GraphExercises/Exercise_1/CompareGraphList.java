package GraphExercises.Exercise_1;

import Grafos.GraphDirected;
import GraphExercises.Graph;

public class CompareGraphList {
    public boolean compareTo(GraphDirected a, GraphDirected b) {
        if (a.getAdjList().size() != b.getAdjList().size()) {
            System.out.println("aren't equals");
            return false;
        }

        for (int i = 0; i < a.getAdjList().size(); i++) {
            for (int j = 0; j < a.getAdjList().get(i).size(); j++) {
                if (a.getAdjList().get(i).size() != b.getAdjList().get(i).size()) {
                    System.out.println("aren't equals");
                    return false;
                }

                if (a.getAdjList().get(i).get(j) != b.getAdjList().get(i).get(j)) {
                    System.out.println("aren't equals");
                    return false;
                }
            }
        }
        System.out.println("are equals");
        return true;
    }

    public boolean compareTo(Graph a, Graph b) {
        if (a.getAdjList().size() != b.getAdjList().size()) {
            System.out.println("aren't equals");
            return false;
        }

        for (int i = 0; i < a.getAdjList().size(); i++) {
            for (int j = 0; j < a.getAdjList().get(i).size(); j++) {
                if (a.getAdjList().get(i).size() != b.getAdjList().get(i).size()) {
                    System.out.println("aren't equals");
                    return false;
                }

                if (a.getAdjList().get(i).get(j).getSource() != b.getAdjList().get(i).get(j).getSource()
                        && a.getAdjList().get(i).get(j).getDestination() != b.getAdjList().get(i).get(j).getDestination()){
                    System.out.println("aren't equals");
                    return false;
                }
            }
        }
        System.out.println("are equals");
        return true;
    }
}
