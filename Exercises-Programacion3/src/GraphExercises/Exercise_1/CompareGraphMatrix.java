package GraphExercises.Exercise_1;

import Grafos.GraphMatrix;
import TypeGraph.Matrix.UndirectedGraph;

public class CompareGraphMatrix {
    public boolean compareTo(UndirectedGraph a, UndirectedGraph b) {

        if (a.getGraph().length != b.getGraph().length) {
            System.out.println("aren't equals");
            return false;
        }

        for(int i = 0; i < a.getGraph().length; i++) {
            for(int j = 0; j < a.getGraph().length; j++) {
                if (a.getGraph()[i][j] != b.getGraph()[i][j]) {
                    System.out.println("aren't equals");
                    return false;
                    // break;
                }
            }
        }
        System.out.println("are equals");
        return true;
    }

    public boolean compareTo(GraphMatrix a, GraphMatrix b) {

        if (a.getGraph().length != b.getGraph().length) {
            System.out.println("aren't equals");
            return false;
        }

        for(int i = 0; i < a.getGraph().length; i++) {
            for(int j = 0; j < a.getGraph().length; j++) {
                if (a.getGraph()[i][j] != b.getGraph()[i][j]) {
                    System.out.println("aren't equals");
                    return false;
                    // break;
                }
            }
        }
        System.out.println("are equals");
        return true;
    }
}
