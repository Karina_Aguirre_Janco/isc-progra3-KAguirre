package GraphExercises.Exercise_1;

import Grafos.EdgeDirected;
import Grafos.GraphDirected;
import Grafos.GraphMatrix;
import GraphExercises.Edge;
import GraphExercises.Graph;
import TypeGraph.Matrix.UndirectedGraph;

public class Main {
    public static void main(String[] args) {
        //exercise 1 -----------------a----------------------------
        UndirectedGraph graphMatrix = new UndirectedGraph(6,7);
        graphMatrix.fill();
        graphMatrix.addEdge(1,2);
        graphMatrix.addEdge(1,3);
        graphMatrix.addEdge(2,4);
        graphMatrix.addEdge(3,4);
        graphMatrix.addEdge(3,5);
        graphMatrix.addEdge(4,5);
        graphMatrix.addEdge(4,6);

        UndirectedGraph graphMatrix2 = new UndirectedGraph(6,7);
        graphMatrix2.fill();
        graphMatrix2.addEdge(1,2);
        graphMatrix2.addEdge(1,3);
        graphMatrix2.addEdge(2,4);
        graphMatrix2.addEdge(3,4);
        graphMatrix2.addEdge(3,5);
        graphMatrix2.addEdge(4,5);
        graphMatrix2.addEdge(4,6);

        CompareGraphMatrix compareGraphMatrix = new CompareGraphMatrix();
        compareGraphMatrix.compareTo(graphMatrix,graphMatrix2);

        //exercise 1 -----------------b----------------------------
        System.out.println("graph 1:");
        System.out.print("number of vertices: ");
        GraphMatrix graphMatrix3 = new GraphMatrix();
        graphMatrix3.fill();
        System.out.print("the number of relations are: ");
        graphMatrix3.addEdge();
        System.out.println("The matrix is: ");
        graphMatrix3.printMatrix();
        System.out.println("graph 2:");
        System.out.print("number of vertices: ");
        GraphMatrix graphMatrix4 = new GraphMatrix();
        graphMatrix4.fill();
        System.out.print("the number of relations are: ");
        graphMatrix4.addEdge();
        System.out.println("The matrix is: ");
        graphMatrix4.printMatrix();
        compareGraphMatrix.compareTo(graphMatrix3,graphMatrix4);
    }
}

class MainAdjList{
    public static void main(String[] args) {
        //exercise 1 -----------------a----------------------------
        Graph graph2 = new Graph(6);
        graph2.addEdge(new Edge<>(1,2));
        graph2.addEdge(new Edge<>(1,3));
        graph2.addEdge(new Edge<>(2,4));
        graph2.addEdge(new Edge<>(4,6));
        graph2.addEdge(new Edge<>(4,5));
        graph2.addEdge(new Edge<>(3,4));
        graph2.addEdge(new Edge<>(5,3));

        Graph graph3 = new Graph(6);
        graph3.addEdge(new Edge<>(1,2));
        graph3.addEdge(new Edge<>(1,3));
        graph3.addEdge(new Edge<>(2,4));
        graph3.addEdge(new Edge<>(4,6));
        graph3.addEdge(new Edge<>(4,5));
        graph3.addEdge(new Edge<>(3,4));
        graph3.addEdge(new Edge<>(5,3));

        CompareGraphList compareGraphList = new CompareGraphList();
        compareGraphList.compareTo(graph2, graph3);
        //exercise 1 -----------------b----------------------------
        GraphDirected graph = new GraphDirected(5);
        graph.addEdge(new EdgeDirected<>(2,1));
        graph.addEdge(new EdgeDirected<>(3,5));
        graph.addEdge(new EdgeDirected<>(3,2));
        graph.addEdge(new EdgeDirected<>(4,3));
        graph.addEdge(new EdgeDirected<>(5,2));

        GraphDirected graph1 = new GraphDirected(5);
        graph1.addEdge(new EdgeDirected<>(2,1));
        graph1.addEdge(new EdgeDirected<>(3,5));
        graph1.addEdge(new EdgeDirected<>(3,2));
        graph1.addEdge(new EdgeDirected<>(5,4));
        graph1.addEdge(new EdgeDirected<>(5,2));

        CompareGraphList compareGraphList2 = new CompareGraphList();
        compareGraphList2.compareTo(graph,graph1);
    }
}
