package GraphExercises.Exercise_3;

public class StronglyConnectedMatrixGraph {
    private int[][] graph;
    private boolean[] visited;

    public StronglyConnectedMatrixGraph(int [][] graph) {
        this.graph = graph;
        visited = new boolean[this.graph.length];
    }

    private void DFS(int vertex) {
        visited[vertex] = true;
        for (int i = 0; i < graph.length; i++) {
            if (graph[vertex][i] == 1 && !visited[i]) {
                DFS(i);
            }
        }
    }

    public boolean isStronglyConnected() {
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph.length; j++) {
                visited[j] = false;
            }

            DFS(i);

            for (int j = 0; j < graph.length; j++) {
                if (visited[j] == false && i != j) {
                    return false;
                }
            }

            for (int j = 0; j < graph.length; j++) {
                for (int k = 0; k < graph.length; k++) {
                    visited[k] = false;
                }

                DFS(j);

                for (int k = 0; k < graph.length; k++) {
                    if (visited[k] == false && j != k) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
