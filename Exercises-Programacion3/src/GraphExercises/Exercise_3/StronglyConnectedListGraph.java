package GraphExercises.Exercise_3;

import Grafos.EdgeDirected;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StronglyConnectedListGraph {
    private int V;
    private List<List<EdgeDirected>> adj;
    private List<EdgeDirected> edges;

    public StronglyConnectedListGraph(int v, int e ,List<List<EdgeDirected>> adj ) {
        V = v;
        this.adj = adj;
        edges = new ArrayList<>(e);
    }

    private void DFS(int v, Stack<Integer> stack) {
        stack.push(v);
        for (EdgeDirected edge : adj.get(v)) {
            int i = (int) edge.getDestination();
            if (!stack.contains(i)) {
                DFS(i, stack);
            }
        }
    }

    public boolean isStronglyConnected() {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < V; i++) {
            if (!stack.contains(i)) {
                DFS(i, stack);
            }
        }

        adj = new ArrayList<>(V);
        for (int i = 0; i < V; i++) {
            adj.add(new ArrayList<>());
        }

        for (EdgeDirected edge : edges) {
            adj.get((Integer) edge.getDestination()).add(new EdgeDirected(edge.getSource(),edge.getDestination()));
        }

        Stack<Integer> reverseStack = new Stack<>();
        for (int i = 0; i < V; i++) {
            if (!reverseStack.contains(i)) {
                DFS(i, reverseStack);
            }
        }

        return stack.size() == reverseStack.size();
    }
}
