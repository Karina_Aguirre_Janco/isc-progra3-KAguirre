package GraphExercises.Exercise_3;

import Grafos.EdgeDirected;
import Grafos.GraphDirected;
import Grafos.GraphMatrix;

public class Main {
    public static void main(String[] args) {
        System.out.print("number of vertices: ");
        GraphMatrix graphMatrix = new GraphMatrix();
        graphMatrix.fill();
        System.out.print("the number of relations are: ");
        graphMatrix.addEdge();
        System.out.println("The matrix is: ");
        graphMatrix.printMatrix();

        StronglyConnectedMatrixGraph stronglyConnectedGraph = new StronglyConnectedMatrixGraph(graphMatrix.getGraph());
        if (stronglyConnectedGraph.isStronglyConnected()){
            System.out.println("The graph is strongly connected");
        } else {
            System.out.println("The graph is not strongly connected");
        }
    }
}

class MainAdjList{
    public static void main(String[] args) {
        System.out.println("-----Exercise 1---------");
        GraphDirected graphDirected = new GraphDirected(5);
        graphDirected.addEdge(new EdgeDirected<>(2,1));
        graphDirected.addEdge(new EdgeDirected<>(1,4));
        graphDirected.addEdge(new EdgeDirected<>(5,1));
        graphDirected.addEdge(new EdgeDirected<>(4,5));
        graphDirected.addEdge(new EdgeDirected<>(3,2));
        graphDirected.addEdge(new EdgeDirected<>(4,3));
        graphDirected.addEdge(new EdgeDirected<>(3,5));
        graphDirected.addEdge(new EdgeDirected<>(5,3));

        StronglyConnectedListGraph stronglyConnectedListGraph = new StronglyConnectedListGraph(graphDirected.getAdjList().size(),
                graphDirected.getEdges(), graphDirected.getAdjList());
        if (stronglyConnectedListGraph.isStronglyConnected()){
            System.out.println("The graph is strongly connected");
        } else {
            System.out.println("The graph is not strongly connected");
        }
    }
}

