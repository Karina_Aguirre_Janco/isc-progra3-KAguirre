package GraphExercises;

public class GraphMatrix {
    private int relations;
    private int [][] graph;

    public GraphMatrix(int sizeMatrix, int relations) {
        this.relations=relations;
        graph = new int[sizeMatrix][sizeMatrix];
    }

    public int [][] fill(){
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                graph[i][j] = 0;
            }
        }
        return graph;
    }

    public int getRelations() {
        return relations;
    }

    public int[][] getGraph() {
        return graph;
    }
}