package GraphExercises;

public class UndirectedGraph extends GraphMatrix {
    public UndirectedGraph(int sizeMatrix, int relations) {
        super(sizeMatrix, relations);
    }

    public void addEdge(int firstNumber, int secondNumber) {
        for (int i = 0; i < getRelations(); i++) {

            getGraph()[firstNumber - 1][secondNumber - 1] = 1;
            getGraph()[secondNumber - 1][firstNumber - 1] = 1;

        }
    }
}
