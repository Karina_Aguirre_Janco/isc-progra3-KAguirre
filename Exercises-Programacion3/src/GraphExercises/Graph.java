package GraphExercises;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<List<Edge>> adjList;
    public Graph(int numVertexes){
        adjList = new ArrayList<>();
        for(int i =0;i<= numVertexes ; i++){
            adjList.add(i,new ArrayList<>());
        }
    }

    public void addEdge(Edge<Integer> edge){
        adjList.get(edge.getSource()).add(new Edge<>(edge.getSource(),edge.getDestination(),edge.getWeight()));
        adjList.get(edge.getDestination()).add(new Edge<>(edge.getDestination(),edge.getSource(),edge.getWeight()));
    }

    public List<List<Edge>> getAdjList() {
        return adjList;
    }
}
