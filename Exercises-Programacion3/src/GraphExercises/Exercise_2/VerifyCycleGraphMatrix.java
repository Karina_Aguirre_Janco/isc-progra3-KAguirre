package GraphExercises.Exercise_2;

import GraphExercises.UndirectedGraph;

public class VerifyCycleGraphMatrix {
    private UndirectedGraph graph;
    private boolean[] visited;
    private int[] cycle;
    private int cycleIndex;

    public VerifyCycleGraphMatrix(UndirectedGraph graph) {
        this.graph = graph;
        visited = new boolean[graph.getGraph().length];
        cycle = new int[graph.getGraph().length];
    }

    /*private int vertices;
        private int[][] adjList;

        public VerifyCycleGraphMatrix(int v, int[][] adjList) {
            this.vertices = v;
            this.adjList = adjList;
        }

        public boolean isCyclicUtil(int v, boolean[] visited, int parent) {
            visited[v] = true;

            for (int i = 0; i < vertices; i++) {
                if (adjList[v][i] == 1) {
                    if (!visited[i]) {
                        if (isCyclicUtil(i, visited, v))
                            return true;
                    }
                    else if (i != parent)
                        return true;
                }
            }
            return false;
        }

        public boolean isCyclic() {
            boolean[] visited = new boolean[vertices];

            for (int i = 0; i < vertices; i++)
                visited[i] = false;

            for (int u = 0; u < vertices; u++)
                if (!visited[u])
                    if (isCyclicUtil(u, visited, -1))
                        return true;

            return false;
        }*/
    public int countCycles() {
        int count = 0;
        for (int i = 0; i < graph.getGraph().length; i++) {
            visited[i] = true;
            count += dfs(i, i);
            visited[i] = false;
        }
        return count;
    }

    private int dfs(int start, int current) {
        int count = 0;
        for (int next = 0; next < graph.getGraph()[current].length; next++) {
            if (graph.getGraph()[current][next] != 0) {
                if (next == start && cycleIndex > 1) {
                    count++;
                    cycle[cycleIndex++] = start;
                    printCycle();
                    cycleIndex--;
                } else if (!visited[next]) {
                    visited[next] = true;
                    cycle[cycleIndex++] = next;
                    count += dfs(start, next);
                    visited[next] = false;
                    cycleIndex--;
                }
            }
        }
        return count;
    }

    public void printCycle() {
        System.out.print("Cycle: ");
        for (int i = 0; i < cycleIndex; i++) {
            System.out.print(cycle[i] + " ");
        }
        System.out.println();
    }
}
