package GraphExercises.Exercise_2;

import GraphExercises.Edge;
import GraphExercises.Graph;
import java.util.ArrayList;
import java.util.List;

public class VerifyCycleGraphList {
    private int vertices;
    private boolean[] visited;
    private List<Integer> cycle;
    private Graph graph;

    public VerifyCycleGraphList(Graph graph) {
        this.graph = graph;
        this.vertices = graph.getAdjList().size();
        this.visited = new boolean[vertices];
        this.cycle = new ArrayList<>();
    }

    public int countCycles() {
        int count = 0;
        for (int i = 0; i < vertices; i++) {
            visited[i] = true;
            count += dfs(i, i);
            visited[i] = false;
        }
        return count;
    }

    public int dfs(int start, int current) {
        int count = 0;
        for (Edge edge : graph.getAdjList().get(current)) {
            int next = (int) edge.getDestination();
            if (next == start && cycle.size() > 1) {
                count++;
                cycle.add(start);
                printCycle();
                cycle.remove(cycle.size() - 1);
            } else if (!visited[next]) {
                visited[next] = true;
                cycle.add(next);
                count += dfs(start, next);
                visited[next] = false;
                cycle.remove(cycle.size() - 1);
            }
        }
        return count;
    }

    public void printCycle() {
        System.out.print("Cycle: ");
        for (int node : cycle) {
            System.out.print(node + " ");
        }
        System.out.println();
    }
}