package GraphExercises.Exercise_2;

import GraphExercises.Edge;
import GraphExercises.Graph;
import GraphExercises.UndirectedGraph;

public class Main {
    public static void main(String[] args) {
        UndirectedGraph graphMatrix = new UndirectedGraph(6,7);
        graphMatrix.fill();
        graphMatrix.addEdge(1,2);
        graphMatrix.addEdge(1,3);
        graphMatrix.addEdge(2,4);
        graphMatrix.addEdge(3,4);
        graphMatrix.addEdge(3,5);
        graphMatrix.addEdge(4,5);
        graphMatrix.addEdge(4,6);

        VerifyCycleGraphMatrix verifyCycleGraphMatrix = new VerifyCycleGraphMatrix(graphMatrix);
        verifyCycleGraphMatrix.printCycle();
        System.out.println("Paths Number: " + verifyCycleGraphMatrix.countCycles());

        UndirectedGraph graphMatrix2 = new UndirectedGraph(4,2);
        graphMatrix2.fill();
        graphMatrix2.addEdge(1,2);
        graphMatrix2.addEdge(2,3);

        VerifyCycleGraphMatrix verifyCycleGraphMatrix2 = new VerifyCycleGraphMatrix(graphMatrix2);
        verifyCycleGraphMatrix2.printCycle();
        System.out.println("Paths Number: " + verifyCycleGraphMatrix2.countCycles());
    }
}

class MainAdjList{
    public static void main(String[] args) {
        Graph graph2 = new Graph(6);
        graph2.addEdge(new Edge<>(1,2));
        graph2.addEdge(new Edge<>(1,3));
        graph2.addEdge(new Edge<>(2,4));
        graph2.addEdge(new Edge<>(4,6));
        graph2.addEdge(new Edge<>(4,5));
        graph2.addEdge(new Edge<>(3,4));
        graph2.addEdge(new Edge<>(5,3));

        System.out.println("exercise a------------------------");
        VerifyCycleGraphList verifyCycleGraphList = new VerifyCycleGraphList(graph2);
        verifyCycleGraphList.printCycle();
        System.out.println("Paths Number: " + verifyCycleGraphList.countCycles());

        Graph graph3 = new Graph(4);
        graph3.addEdge(new Edge<>(1,2));
        graph3.addEdge(new Edge<>(2,3));

        System.out.println("exercise b------------------------");
        VerifyCycleGraphList verifyCycleGraphList2 = new VerifyCycleGraphList(graph3);
        verifyCycleGraphList2.printCycle();
        System.out.println("Paths Number: " + verifyCycleGraphList2.countCycles());

    }
}

