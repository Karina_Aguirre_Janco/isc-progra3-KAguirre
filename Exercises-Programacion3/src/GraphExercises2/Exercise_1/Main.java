package GraphExercises2.Exercise_1;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        String[] courses = {"Algebra","Geometry", "Statistics", "Trigonometry", "Calculus","Machine Learning"};
        String[][] prerequisites = {
                {"Algebra","Geometry"},
                {"Algebra", "Statistics"},
                {"Geometry", "Trigonometry"},
                {"Statistics","Machine Learning"},
                {"Trigonometry", "Calculus"},
                {"Calculus", "Machine Learning"}};

        CourseGraph graph = new CourseGraph(6, prerequisites, courses);

        FindPrerequisites findPrerequisites = new FindPrerequisites(graph);
        System.out.println("Exercise_1------------------------------");
        findPrerequisites.findPaths("Machine Learning", "Algebra");
        System.out.println("Exercise_2------------------------------");
        findPrerequisites.findPaths("Statistics", "Algebra");
    }
}
