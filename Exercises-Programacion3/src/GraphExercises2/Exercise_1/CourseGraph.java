package GraphExercises2.Exercise_1;

import java.util.*;

public class CourseGraph {
    private int numCourses;
    private String[] courseNames;
    private Map<String, List<String>> adjList;
    private Map<String, Integer> courseIndices;

    public CourseGraph(int numCourses, String[][] prerequisites,String[]courseNames) {
        this.numCourses = numCourses;
        this.courseNames = courseNames;
        this.adjList = new HashMap<>();
        this.courseIndices = new HashMap<>();

        for (int i = 0; i < numCourses; i++) {
            adjList.put(courseNames[i], new ArrayList<>());
            courseIndices.put(courseNames[i], i);
        }

        for (String[] edge : prerequisites) {
            String src = edge[1];
            String destination = edge[0];
            adjList.get(src).add(destination);
        }
    }

    public int getNumCourses() {
        return adjList.size();
    }

    public Map<String, List<String>> getAdjList() {
        return adjList;
    }
    public Map<String, Integer> getCourseIndices() {
        return courseIndices;
    }
}