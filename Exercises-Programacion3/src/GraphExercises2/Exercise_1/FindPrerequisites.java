package GraphExercises2.Exercise_1;

import java.util.ArrayList;
import java.util.List;

public class FindPrerequisites {
    CourseGraph courseGraph;

    public FindPrerequisites(CourseGraph courseGraph) {
        this.courseGraph = courseGraph;
    }

    private void DFS(String start, String end, boolean[] visited, List<String> path, List<List<String>> paths) {
        visited[courseGraph.getCourseIndices().get(start)] = true;
        path.add(start);

        if (start.equals(end)) {
            paths.add(new ArrayList<>(path));
            System.out.println(String.join(" -> ", path));
        } else {
            for (String neighbor : courseGraph.getAdjList().get(start)) {
                if (!visited[courseGraph.getCourseIndices().get(neighbor)]) {
                    DFS(neighbor, end, visited, path, paths);
                }
            }
        }

        path.remove(path.size() - 1);
        visited[courseGraph.getCourseIndices().get(start)] = false;
    }

    public void findPaths(String start, String end) {
        boolean[] visited = new boolean[courseGraph.getNumCourses()];
        List<String> path = new ArrayList<>();
        List<List<String>> paths = new ArrayList<>();

        DFS(start, end, visited, path, paths);
    }
}