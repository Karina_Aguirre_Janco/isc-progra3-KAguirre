package GraphExercises2.Exercise_2;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph(8);
        graph.addEdge(1, 4, 0.23f);
        graph.addEdge(1, 2, 0.35f);
        graph.addEdge(1, 3, 0.23f);
        graph.addEdge(4, 2, 0.74f);
        graph.addEdge(2, 3, 0.24f);
        graph.addEdge(3, 5, 0.51f);
        graph.addEdge(4, 6, 0.24f);
        graph.addEdge(2, 6, 0.26f);
        graph.addEdge(3, 7, 0.14f);
        graph.addEdge(5, 7, 0.15f);
        graph.addEdge(6, 8, 0.32f);
        graph.addEdge(7, 8, 0.32f);


        Path path = new Path(graph);
        path.shortestPath(1, 8);
        path.printPath();
    }
}
