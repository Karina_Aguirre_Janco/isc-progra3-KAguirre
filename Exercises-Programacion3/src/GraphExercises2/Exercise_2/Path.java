package GraphExercises2.Exercise_2;

import GraphExercises2.Edge;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Path {
    private Graph graph;
    private List<Integer> path;
    private float[] distance;
    private int dest;
    public Path(Graph graph) {
        this.graph = graph;
        path = new ArrayList<>();
    }

    public void DFSUtil(int u, boolean[] visited, float[] distance, int[] parent) {
        visited[u] = true;

        for (int i = 0; i < graph.getAdjList().get(u).size(); i++) {
            Edge edge = graph.getAdjList().get(u).get(i);
            int v = (int) edge.getDestination();
            float w = (float) edge.getWeight();

            if (!visited[v]) {
                distance[v] = distance[u] + w;
                parent[v] = u;
                DFSUtil(v, visited, distance, parent);
            }
        }
    }

    public void shortestPath(int source, int destination) {
        boolean[] visited = new boolean[graph.getAdjList().size()];
        distance = new float[graph.getAdjList().size()];
        int[] parent = new int[graph.getAdjList().size()];

        Arrays.fill(distance, Integer.MAX_VALUE);
        distance[source] = 0;
        parent[source] = -1;

        DFSUtil(source, visited, distance, parent);

        int current = destination;
        while (current != -1) {
            path.add(current);
            current = parent[current];
        }
        dest = destination;
    }

    public void printPath(){
        System.out.print("Path: ");
        for (int i = path.size() - 1; i >= 0; i--) {
            System.out.print(path.get(i));
            if (i != 0) {
                System.out.print(" -> ");
            }
        }

        System.out.println();
        System.out.println("Distance: " + distance[dest]);
    }
}
