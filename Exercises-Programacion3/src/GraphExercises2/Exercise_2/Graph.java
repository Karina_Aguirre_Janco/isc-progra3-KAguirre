package GraphExercises2.Exercise_2;

import GraphExercises2.Edge;
import GraphExercises2.GraphActions;
import java.util.ArrayList;
import java.util.List;

class Graph implements GraphActions {
    private List<List<Edge>> adjList;

    Graph(int numVertices) {
        adjList = new ArrayList<>();

        for (int i = 0; i < numVertices +1; i++) {
            adjList.add(new ArrayList<>());
        }
    }

    @Override
    public void addEdge(int source, int destination, float weight) {
        adjList.get(source).add(new Edge(source, destination, weight));
        adjList.get(destination).add(new Edge(destination, source, weight));
    }

    public List<Edge> edgesInPath(List<Integer> path) {
        List<Edge> edges = new ArrayList<>();
        for (int i = 0; i < path.size() - 1; i++) {
            int u = path.get(i);
            int v = path.get(i + 1);
            for (Edge e : adjList.get(u)) {
                if ((int)e.getDestination() == v) {
                    edges.add(e);
                    break;
                }
            }
        }
        return edges;
    }
    public List<List<Edge>> getAdjList() {
        return adjList;
    }
}
