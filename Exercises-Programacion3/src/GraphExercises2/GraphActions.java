package GraphExercises2;

public interface GraphActions<T> {

    void addEdge(int source, int destination, float weight);
}
