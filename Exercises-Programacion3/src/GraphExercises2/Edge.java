package GraphExercises2;

public class Edge<T>{
    private T source,destination , weight;

    public Edge(T source, T destination){
        this.source = source;
        this.destination = destination;
    }
    public Edge(T source, T destination , T weight){
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }
    public T getSource(){
        return this.source;
    }
    public T getDestination(){
        return this.destination;
    }
    public T getWeight(){
        return this.weight;
    }
    public String toStringSource() {
        return String.valueOf(source);
    }
    public String toStringDestination() {
        return String.valueOf(destination);
    }
    public String toStringWeight() {
        return String.valueOf(weight);
    }
}
