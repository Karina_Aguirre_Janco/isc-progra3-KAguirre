package TypeGraph;

import TypeGraph.Matrix.WeightedGraph;
import TypeGraph.List.Edge;
import TypeGraph.List.Graph;

public class MainWeighted {
    public static void main(String[] args) {
        WeightedGraph weightedGraph = new WeightedGraph(6,7);
        weightedGraph.fill();
        weightedGraph.fillWithWeight(1,2,3);
        weightedGraph.fillWithWeight(1,3,9);
        weightedGraph.fillWithWeight(2,4,7);
        weightedGraph.fillWithWeight(3,4,2);
        weightedGraph.fillWithWeight(4,6,1);
        weightedGraph.fillWithWeight(3,5,5);
        weightedGraph.fillWithWeight(5,6,8);
        System.out.println("-----Matrix--------------");
        weightedGraph.printMatrix();
        System.out.println("-----List----------------");
        Graph weightedGraph1 = new Graph(6);
        weightedGraph1.addEdge(new Edge<>(1,2,3));
        weightedGraph1.addEdge(new Edge<>(1,3,9));
        weightedGraph1.addEdge(new Edge<>(2,4,7));
        weightedGraph1.addEdge(new Edge<>(3,4,2));
        weightedGraph1.addEdge(new Edge<>(4,6,1));
        weightedGraph1.addEdge(new Edge<>(3,5,5));
        weightedGraph1.addEdge(new Edge<>(5,6,8));

        System.out.println(weightedGraph1.toStringWeighted());
    }
}
