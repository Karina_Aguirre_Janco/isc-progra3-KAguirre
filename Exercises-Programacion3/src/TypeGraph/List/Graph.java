package TypeGraph.List;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<List<Edge>> adjList;
    private int edges;

    public Graph(int numVertexes){
        adjList = new ArrayList<>();
        for(int i =0;i<= numVertexes ; i++){
            adjList.add(i,new ArrayList<>());
        }
    }

    public void addEdge(Edge<Integer> edge){
        adjList.get(edge.getSource()).add(new Edge<>(edge.getSource(),edge.getDestination(),edge.getWeight()));
        adjList.get(edge.getDestination()).add(new Edge<>(edge.getDestination(),edge.getSource(),edge.getWeight()));
    }

    public String toStringWeighted(){
        int index = 0;
        StringBuilder sb = new StringBuilder();
        while (index<adjList.size()){
            for (Edge dest : adjList.get(index)){
                sb.append("( "+index+" --> ( "+ dest.toStringDestination() +" | "+ dest.toStringWeight()+ " ) )\t ");
            }
            sb.append("\n");
            index++;
        }
        return sb.toString();
    }

    public String toStringUndirected(){
        int index = 0;
        StringBuilder sb = new StringBuilder();
        while (index<adjList.size()){
            for (Edge dest : adjList.get(index)){
                sb.append("( "+index+" --> ( "+ dest.toStringDestination() + " ) )\t ");
            }
            sb.append("\n");
            index++;
        }
        return sb.toString();
    }

    public List<List<Edge>> getAdjList() {
        return adjList;
    }
}
