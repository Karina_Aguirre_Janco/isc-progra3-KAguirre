package TypeGraph;

import TypeGraph.List.Edge;
import TypeGraph.List.Graph;
import TypeGraph.Matrix.UndirectedGraph;

public class MainUndirected {
    public static void main(String[] args) {

        UndirectedGraph graphMatrix = new UndirectedGraph(5,6);
        graphMatrix.fill();
        graphMatrix.addEdge(1,2);
        graphMatrix.addEdge(2,3);
        graphMatrix.addEdge(3,4);
        graphMatrix.addEdge(3,5);
        graphMatrix.addEdge(4,2);
        graphMatrix.addEdge(1,3);
        System.out.println("-----Matrix----------------");
        graphMatrix.printMatrix();
        System.out.println("-----List----------------");
        Graph graph = new Graph(5);
        graph.addEdge(new Edge<>(1,2));
        graph.addEdge(new Edge<>(2,3));
        graph.addEdge(new Edge<>(3,4));
        graph.addEdge(new Edge<>(3,5));
        graph.addEdge(new Edge<>(4,2));
        graph.addEdge(new Edge<>(1,3));

        System.out.println(graph.toStringUndirected());
    }
}
