package TypeGraph.Matrix;

public class UndirectedGraph extends GraphMatrix {
    private int edges;

    public UndirectedGraph(int sizeMatrix, int relations) {
        super(sizeMatrix, relations);
    }

    public void addEdge(int firstNumber, int secondNumber) {
        for (int i = 0; i < getRelations(); i++) {

            getGraph()[firstNumber - 1][secondNumber - 1] = 1;
            getGraph()[secondNumber - 1][firstNumber - 1] = 1;

        }
    }

    public int getEdges() {
        for (int i = 0; i < getGraph().length; i++) {
            for (int j = 0; j < getGraph()[i].length; j++) {
                if (getGraph()[i][j] != 0){
                    edges++;
                }
            }
        }
        return edges;
    }
}
