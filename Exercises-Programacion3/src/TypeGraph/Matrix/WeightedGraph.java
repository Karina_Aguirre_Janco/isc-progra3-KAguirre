package TypeGraph.Matrix;

public class WeightedGraph extends GraphMatrix {

    public WeightedGraph(int sizeMatrix, int relations) {
        super(sizeMatrix, relations);
    }

    public void fillWithWeight(int firstNumber, int secondNumber, int weight) {
        for (int i = 0; i < getRelations(); i++) {

            getGraph()[firstNumber - 1][secondNumber - 1] = weight;
            getGraph()[secondNumber - 1][firstNumber - 1] = weight;

        }
    }
}
