package TypeGraph.Matrix;

public class GraphMatrix {
    private int sizeMatrix ;
    private int relations;
    private int [][] graph;

    public GraphMatrix(int sizeMatrix, int relations) {
        this.sizeMatrix = sizeMatrix;
        this.relations=relations;
        graph = new int[sizeMatrix][sizeMatrix];
    }

    public int [][] fill(){
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                graph[i][j] = 0;
            }
        }
        return graph;
    }
    public void printMatrix(){
        for (int i = 0; i < graph.length; i++) {
            System.out.print(i+1 +": " );
            for (int j = 0; j < graph[i].length; j++) {
                System.out.print(graph[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getRelations() {
        return relations;
    }

    public int[][] getGraph() {
        return graph;
    }
}
