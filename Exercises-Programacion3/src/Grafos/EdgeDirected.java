package Grafos;

public class EdgeDirected<T>{
    private T source,destination , weight;

    public EdgeDirected(T source, T destination){
        this.source = source;
        this.destination = destination;
    }
    public EdgeDirected(T source, T destination , T weight){
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }
    public T getSource(){
        return this.source;
    }
    public T getDestination(){
        return this.destination;
    }
    public T getWeight(){
        return this.weight;
    }
    public String toStringDestination() {
        return String.valueOf(destination);
    }
    public String toStringWeight() {
        return String.valueOf(weight);
    }
}
