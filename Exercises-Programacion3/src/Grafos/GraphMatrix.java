package Grafos;

import java.util.Scanner;

public class GraphMatrix {
    Scanner sc = new Scanner(System.in);
    private int vertices = sc.nextInt();
    private int edges;
    int [][] graph = new int[vertices][vertices];

    public int [][] fill(){
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                graph[i][j] = 0;
            }
        }
        return graph;
    }

    public void addEdge(){
        edges = sc.nextInt();
        while (edges >0){
            System.out.print("The number: ");
            int firstNumber = sc.nextInt();
            System.out.print("relates with: ");
            int secondNumber = sc.nextInt();

            graph[firstNumber-1][secondNumber-1] = 1;

            edges--;
        }
    }

    public void printMatrix(){
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                System.out.print(graph[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int[][] getGraph() {
        return graph;
    }

    public int getVertices() {
        return vertices;
    }

    public int getEdges() {
        return edges;
    }

    public static void main(String[] args) {
        System.out.print("number of vertices: ");
        GraphMatrix graphMatrix = new GraphMatrix();
        graphMatrix.fill();
        System.out.print("the number of relations are: ");
        graphMatrix.addEdge();
        System.out.println("The matrix is: ");
        graphMatrix.printMatrix();
    }
}
