package Grafos;

public class GraphList {
    public static void main(String[] args) {
        GraphDirected graph = new GraphDirected(5);
        graph.addEdge(new EdgeDirected<>(1,2));
        graph.addEdge(new EdgeDirected<>(2,3));
        graph.addEdge(new EdgeDirected<>(3,4));
        graph.addEdge(new EdgeDirected<>(3,5));
        graph.addEdge(new EdgeDirected<>(4,2));
        graph.addEdge(new EdgeDirected<>(1,3));

        System.out.println(graph.toStringDirected());

    }
}
