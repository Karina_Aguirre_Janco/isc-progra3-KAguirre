package Grafos;

import java.util.ArrayList;
import java.util.List;

public class GraphDirected {
    List<List<EdgeDirected>> adjList;
    private int edges;

    public GraphDirected(int numVertexes){
        adjList = new ArrayList<>();
        for(int i =0;i<= numVertexes ; i++){
            adjList.add(i,new ArrayList<>());
        }
    }

    public void addEdge(EdgeDirected<Integer> edge){
        adjList.get(edge.getSource()).add(new EdgeDirected<>(edge.getSource(),edge.getDestination(),edge.getWeight()));
        edges++;
    }

    public String toStringWeighted(){
        int index = 0;
        StringBuilder sb = new StringBuilder();
        while (index<adjList.size()){
            for (EdgeDirected dest : adjList.get(index)){
                sb.append("( "+index+" --> ( "+ dest.toStringDestination() +" | "+ dest.toStringWeight()+ " ) )\t ");
            }
            sb.append("\n");
            index++;
        }
        return sb.toString();
    }

    public String toStringDirected(){
        int index = 0;
        StringBuilder sb = new StringBuilder();
        while (index<adjList.size()){
            for (EdgeDirected dest : adjList.get(index)){
                sb.append("( "+index+" --> ( "+ dest.toStringDestination() + " ) )\t ");
            }
            sb.append("\n");
            index++;
        }
        return sb.toString();
    }

    public List<List<EdgeDirected>> getAdjList() {
        return adjList;
    }

    public int getEdges() {
        return edges;
    }
}

